/*
 * Copyright (c) 2010 The Pennsylvania State University
 * Systems and Internet Infrastructure Security Laboratory
 *
 * Authors: William Enck <enck@cse.psu.edu>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * dalvik.system.Taint
 */
#include "Dalvik.h"
#include "native/InternalNativePriv.h"
#include "attr/xattr.h"

#include <errno.h>

#define TAINT_XATTR_NAME "user.taint"

/* modified_for_data_analysis */
#include <sys/syscall.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/stat.h>

#define __NR_SYSCALL_BASE       0
#define SYS_gettaint __NR_SYSCALL_BASE+376
#define SYS_settaint __NR_SYSCALL_BASE+377


#define AMNIDROID_DEVICE	"/dev/taintctrl"

/* IOCTL */
#define AMNIDROID_IOC_MAGIC	'G'
#define AMNIDROID_SYSTEM	_IO(AMNIDROID_IOC_MAGIC, 0)
#define AMNIDROID_READ		_IOR(AMNIDROID_IOC_MAGIC, 1, char *)
#define AMNIDROID_WRITE		_IOW(AMNIDROID_IOC_MAGIC, 2, char *)
#define AMNIDROID_GETTAINT	_IOR(AMNIDROID_IOC_MAGIC, 3, int *)
#define AMNIDROID_SETTAINT	_IOW(AMNIDROID_IOC_MAGIC, 4, int *)
#define AMNIDROID_CAPSULE	_IOW(AMNIDROID_IOC_MAGIC, 5, struct capsule_t *)
#define AMNIDROID_CONTEXT_ADD	_IOW(AMNIDROID_IOC_MAGIC, 6, struct context_t *)
#define AMNIDROID_POLICY	_IOW(AMNIDROID_IOC_MAGIC, 7, struct policy_t *)
#define AMNIDROID_EVAL_POLICY	_IOWR(AMNIDROID_IOC_MAGIC, 8, int *)
#define AMNIDROID_SET_NEXT_TAG	_IOW(AMNIDROID_IOC_MAGIC, 9, struct object_t *)
#define AMNIDROID_GET_NEXT_TAG	_IOWR(AMNIDROID_IOC_MAGIC, 10, struct object_t *)
#define AMNIDROID_GET_SOCKET_TAG	_IOWR(AMNIDROID_IOC_MAGIC, 11, struct object_t *)
#define AMNIDROID_SET_SOCKET_TAG	_IOW(AMNIDROID_IOC_MAGIC, 12, struct object_t *)
#define AMNIDROID_WHITELIST_PID	_IOW(AMNIDROID_IOC_MAGIC, 13, int *)
#define AMNIDROID_CONTEXT_TIMEFENCE_ENTER	_IOW(AMNIDROID_IOC_MAGIC, 14, struct context_t *)
#define AMNIDROID_CONTEXT_TIMEFENCE_EXIT	_IOW(AMNIDROID_IOC_MAGIC, 15, struct context_t *)
#define AMNIDROID_CONTEXT_GEOFENCE_ENTER	_IOW(AMNIDROID_IOC_MAGIC, 16, struct context_t *)
#define AMNIDROID_CONTEXT_GEOFENCE_EXIT	_IOW(AMNIDROID_IOC_MAGIC, 17, struct context_t *)

// Test 1 extEnv: Fails in some cases
// JNIEnvExt* extEnv = dvmGetJNIEnvForThread();
// Tag stored in: extEnv->tagfd

/* Swirls taintctrl file descriptor */
int tagFd = 0;

enum TAG_RESPONSE {
  TAG_NONEXISTANT,
  TAG_BLOCK,
  TAG_ALLOW,
  TAG_ALLOW_LOG,
  TAG_UPDATE
};

struct object_t {
	int tag;
	int persist; // for application only, means cannot start as different color
	int64_t idcontext;
	char *name;
};

// #############################################
// Listing file descriptors
// #############################################

#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/resource.h>
//#include <sys/syslimits.h>


#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif

char *fcntl_flags(int flags)
{
	static char output[128];
	*output = 0;

	if (flags & O_RDONLY)
		strcat(output, "O_RDONLY ");
	if (flags & O_WRONLY)
		strcat(output, "O_WRONLY ");
	if (flags & O_RDWR)
		strcat(output, "O_RDWR ");
	if (flags & O_CREAT)
		strcat(output, "O_CREAT ");
	if (flags & O_EXCL)
		strcat(output, "O_EXCL ");
	if (flags & O_NOCTTY)
		strcat(output, "O_NOCTTY ");
	if (flags & O_TRUNC)
		strcat(output, "O_TRUNC ");
	if (flags & O_APPEND)
		strcat(output, "O_APPEND ");
	if (flags & O_NONBLOCK)
		strcat(output, "O_NONBLOCK ");
	if (flags & O_SYNC)
		strcat(output, "O_SYNC ");
	if (flags & O_ASYNC)
		strcat(output, "O_ASYNC ");

	return output;
}

char *fd_info(int fd)
{
	if (fd < 0 || fd >= FD_SETSIZE)
		return FALSE;
	// if (fcntl(fd, F_GETFL) == -1 && errno == EBADF)
	int rv = fcntl(fd, F_GETFL);
	return (rv == -1) ? strerror(errno) : fcntl_flags(rv);
}


/* implementation of Donal Fellows method */ 
int get_num_fds()
{
	int fd_count;
	char buf[64];
	struct dirent *dp;

	snprintf(buf, 256, "/proc/%i/fd/", getpid());

	fd_count = 0;
	DIR *dir = opendir(buf);
	while ((dp = readdir(dir)) != NULL) {
		fd_count++;
	}
	closedir(dir);
	return fd_count;
}

rlim_t get_rlimit_files()
{
    struct rlimit rlim;
    getrlimit(RLIMIT_NOFILE, &rlim);
    return rlim.rlim_cur;
}

/* check whether a file-descriptor is valid */
int pth_util_fd_valid(int fd)
{
	//if (fd < 3 || fd >= FD_SETSIZE)
	if (fd < 3 || fd >= get_rlimit_files())
		return FALSE;
	if (fcntl(fd, F_GETFL) == -1 && errno == EBADF)
		return FALSE;
	return TRUE;
}

/* check first 1024 (usual size of FD_SESIZE) file handles */
int test_fds()
{
	int i;
	int fd_dup;
	char errst[64];
	char fdPath[PATH_MAX];
	char filePath[PATH_MAX];
	//for (i = 0; i < FD_SETSIZE; i++) {
	for (i = 0; i < get_rlimit_files(); i++) {
		*errst = 0;
		fd_dup = dup(i);
		if (fd_dup == -1) {
			strcpy(errst, strerror(errno));
			// EBADF  oldfd isnâ€™t an open file descriptor, or newfd is out of the allowed range for file descriptors.
			// EBUSY  (Linux only) This may be returned by dup2() during a race condition with open(2) and dup().
			// EINTR  The dup2() call was interrupted by a signal; see signal(7).
			// EMFILE The process already has the maximum number of file descriptors open and tried to open a new one.
		} else {
			close(fd_dup);
			strcpy(errst, "dup() ok");
		}
		bzero(fdPath, PATH_MAX);
		bzero(filePath, PATH_MAX);
		sprintf(fdPath, "/proc/self/fd/%i", i);
		readlink(fdPath, filePath, PATH_MAX);
	//	if (fcntl(i, F_GETPATH, filePath) == -1)
	//	{
	//		filePath="filename not available";
	//	}
		ALOGE("%4i: %5i %24s %24s %s\n", i, fcntl(i, F_GETOWN), fd_info(i), errst, filePath);
	}
	return 0;
}

// #############################################

int open_taintctrl_dev()
{
//	int ret = 0;
	//if (tagFd == NULL){
		ALOGD("Taintlog: creating the file descriptor for process %d", getpid());
		//tagFd = (int *) malloc(sizeof(int));
		//if (tagFd != NULL )
		//	*tagFd = 1;
		//else
		//	ALOGE("Taintlog: Failure while allocateing memory");
	//}

	if ((tagFd = open(AMNIDROID_DEVICE, O_RDWR)) < 0) {
		ALOGE("Taintlog: open_taintctrl_dev failure while opening taintctrl device");
		//extEnv->tagfd = 0;
		tagFd = 0;
		goto result;
	}
result:
	//test_fds();
	ALOGD("Taintlog: open_taintctrl_dev return from function: %d", tagFd);
	return tagFd;

//	if ( (gDvm.zygote == NULL || (gDvm.zygote != NULL && gDvm.zygote)) || 
//	(gDvm.systemServerPid == NULL || ( gDvm.systemServerPid != NULL && gDvm.systemServerPid != getpid()))) {
//		ALOGD(" ****************** Zygote(%d) or SystemServer(%s) condition ********************", gDvm.zygote, gDvm.systemServerPid);
//		return ret;
//	}
//
//	JNIEnvExt* extEnv = dvmGetJNIEnvForThread();
//	if (extEnv == NULL) {
//		ALOGD(" *********************************** dvmLateEnableCheckedJni: thread has no JNIEnv");
//		ret = 0;
//		goto result;
//	}
//	if (gDvmFd == NULL)
//		gDvmFd = (int*) malloc(sizeof(int));
//
//	if ((ret = open(AMNIDROID_DEVICE, O_RDWR)) < 0) {
//		//ALOGD("Taintlog: open_taintctrl_dev failure while opening taintctrl device %d", ret);
//		//extEnv->tagfd = 0;
//		ret = 0;
//		goto result;
//	}
//	else {
//		//ALOGD("Taintlog: open_taintctrl_dev *********************************** extEnv->tagfd = %d", *gDvmFd);
//	}
//result:
////	return extEnv->tagfd;
//	return ret;
}

int get_taintctrl_dev_fd()
{
//	int ret = 0;
	
	if (tagFd > 0) {
		return tagFd;
	}
	else
		return open_taintctrl_dev();
//	if (ret < 0)
//		ret = 0;
//	return ret;
//
//
//	JNIEnvExt* extEnv = dvmGetJNIEnvForThread();
//	if (extEnv != NULL){
//
//		if (extEnv->tagfd > 0) {
//		if (gDvmFd != NULL && *gDvmFd > 0) {
//			//ALOGE("-.-");
//			//ret = extEnv->tagfd;
//			ret = *gDvmFd;
//		}
//		else {
//			//ALOGE("-X-");
//			ret = open_taintctrl_dev();
//		}
//	}
//	else
//		ALOGE(" *********************************** dvmLateEnableCheckedJni: thread has no JNIEnv");
//	return ret;
}

static void Dalvik_dalvik_system_Taint_getpid(const u4* args,
    JValue* pResult) {
    u4 pid = getpid();
    RETURN_INT(pid);
}

int checkpolicy(int from, int to, int action)
{
	//return -2;
	if (from == to)
		return TAG_ALLOW;

	int fd;
	int param[3];
	param[0] = from;
	param[1] = to;
	param[2] = action;

//	if ((fd = open(AMNIDROID_DEVICE, O_RDWR)) < 0) {
//		ALOGE("Taintlog: checkpolicy failure while opening taintctrl device %d", errno);
//		return -1;
//	}

	if (!(fd=get_taintctrl_dev_fd()))
		return TAG_ALLOW;

	//ALOGI("Taintlog: checkpolicy success while opening taintctrl device");
	if(ioctl(fd, AMNIDROID_EVAL_POLICY, param) < 0){
		ALOGE("Taintlog: checkpolicy failure while requesting taintctrl device received policy %d", param[0]);
		return -1;
	}
//	close(fd);
//	if (! close(fd))
//		ALOGE("Taintlog: checkpolicy failure while closing device");

	return param[0];
}

bool isAllowed(int from, int to)
{
        bool ret = false;
        int action = checkpolicy(from, to, 0);
//        ALOGI("taintlog: **************** isAllowed: policy returned %d", action);

        switch (action)
        {
            case TAG_NONEXISTANT:
                ret = false;
                break;
            case TAG_BLOCK:
                ret = false;
                break;
            case TAG_ALLOW:
                ret = true;
                break;
            case TAG_ALLOW_LOG:
                ret = true;
                // TODO: Log everything here
                break;
            case TAG_UPDATE:
                ret = false;
                break;
            case -1:
                ret = false; // device no ready
                break;
            default:
                ret = true;
                break;
        }
        return ret;
}
/* end_of_modifications */

/*
 * public static void addTaintString(String str, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintString(const u4* args,
    JValue* pResult)
{
    StringObject *strObj = (StringObject*) args[0];
    u4 tag = args[1];
    char * s = dvmCreateCstrFromString(strObj);
    ArrayObject *value = NULL;

    if (strObj) {
    value = strObj->array();
        if ( ! isAllowed(tag, value->taint.tag)){
	    ALOGW("TaintLog: Mixing taints in addTaintString %08x and %08x: ########### %s ###########", value->taint.tag, tag, s);
	}
	value->taint.tag |= tag;
    }
    free(s);
    RETURN_VOID();
}

/*
 * public static void addTaintObjectArray(Object[] array, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintObjectArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    u4 tag = args[1];
    if (arr) {
    if ( ! isAllowed(tag, arr->taint.tag)){
	ALOGW("TaintLog: Mixing taints in addTaintObjectArray %08x and %08x", arr->taint.tag, tag);
    }
	arr->taint.tag |= tag;
    }
    RETURN_VOID();
}

/*
 * public static void addTaintBooleanArray(boolean[] array, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintBooleanArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    u4 tag = args[1];
    if (arr) {
    if ( ! isAllowed(tag, arr->taint.tag)){
	ALOGW("TaintLog: Mixing taints in addTaintBooleanArray %08x and %08x", arr->taint.tag, tag);
    }
	arr->taint.tag |= tag;
    }
    RETURN_VOID();
}

/*
 * public static void addTaintCharArray(char[] array, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintCharArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    u4 tag = args[1];
    if (arr) {
    if ( ! isAllowed(tag, arr->taint.tag)){
	ALOGW("TaintLog: Mixing taints in addTaintCharArray %08x and %08x", arr->taint.tag, tag);
    }
	arr->taint.tag |= tag;
    }
    RETURN_VOID();
}

/*
 * public static void addTaintByteArray(byte[] array, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintByteArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    u4 tag = args[1];
    if (arr) {
    if ( ! isAllowed(tag, arr->taint.tag)){
	ALOGW("TaintLog: Mixing taints in addTaintByteArray %08x and %08x", arr->taint.tag, tag);
    }
	arr->taint.tag |= tag;
    }
    RETURN_VOID();
}

/*
 * public static void addTaintIntArray(int[] array, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintIntArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    u4 tag = args[1];
    if (arr) {
    if ( ! isAllowed(tag, arr->taint.tag)){
	ALOGW("TaintLog: Mixing taints in addTaintIntArray %08x and %08x", arr->taint.tag, tag);
    }
	arr->taint.tag |= tag;
    }
    RETURN_VOID();
}

/*
 * public static void addTaintShortArray(short[] array, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintShortArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    u4 tag = args[1];
    if (arr) {
    if ( ! isAllowed(tag, arr->taint.tag)){
	ALOGW("TaintLog: Mixing taints in addTaintShortArray %08x and %08x", arr->taint.tag, tag);
      }
	arr->taint.tag |= tag;
    }
    RETURN_VOID();
}

/*
 * public static void addTaintLongArray(long[] array, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintLongArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    u4 tag = args[1];
    if (arr) {
    if ( ! isAllowed(tag, arr->taint.tag)){
	ALOGW("TaintLog: Mixing taints in addTaintLongArray %08x and %08x", arr->taint.tag, tag);
      }
	arr->taint.tag |= tag;
    }
    RETURN_VOID();
}

/*
 * public static void addTaintFloatArray(float[] array, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintFloatArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    u4 tag = args[1];
    if (arr) {
    if ( ! isAllowed(tag, arr->taint.tag)){
	ALOGW("TaintLog: Mixing taints in addTaintFloatArray %08x and %08x", arr->taint.tag, tag);
      }
	arr->taint.tag |= tag;
    }
    RETURN_VOID();
}

/*
 * public static void addTaintDoubleArray(double[] array, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintDoubleArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    u4 tag = args[1];
    if (arr) {
    if ( ! isAllowed(tag, arr->taint.tag)){
	ALOGW("TaintLog: Mixing taints in addTaintDouble %08x and %08x", arr->taint.tag, tag);
      }
	arr->taint.tag |= tag;
    }
    RETURN_VOID();
}

/*
 * public static boolean addTaintBoolean(boolean val, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintBoolean(const u4* args,
    JValue* pResult)
{
    u4 val     = args[0];
    u4 tag     = args[1];	 /* the tag to add */
    u4* rtaint = (u4*) &args[2]; /* pointer to return taint tag */
    u4 vtaint  = args[3];	 /* the existing taint tag on val */
    if ( ! isAllowed(tag, vtaint)){
      ALOGW("TaintLog: Mixing taints in addTaintBoolean %08x and %08x", vtaint, tag);
    }
    *rtaint = (vtaint | tag);
    RETURN_BOOLEAN(val);
}

/*
 * public static char addTaintChar(char val, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintChar(const u4* args,
    JValue* pResult)
{
    u4 val     = args[0];
    u4 tag     = args[1];         /* the tag to add */
    u4* rtaint = (u4*) &args[2];  /* pointer to return taint tag */
    u4 vtaint  = args[3];	  /* the existing taint tag on val */
    if ( ! isAllowed(tag, vtaint)){
      ALOGW("TaintLog: Mixing taints in addTaintChar %08x and %08x", vtaint, tag);
    }
    *rtaint = (vtaint | tag);
    RETURN_CHAR(val);
}

/*
 * public static char addTaintByte(byte val, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintByte(const u4* args,
    JValue* pResult)
{
    u4 val     = args[0];
    u4 tag     = args[1];         /* the tag to add */
    u4* rtaint = (u4*) &args[2];  /* pointer to return taint tag */
    u4 vtaint  = args[3];	  /* the existing taint tag on val */
    if ( ! isAllowed(tag, vtaint)){
      ALOGW("TaintLog: Mixing taints in addTaintByte %08x and %08x", vtaint, tag);
    }
    *rtaint = (vtaint | tag);
    RETURN_BYTE(val);
}

/*
 * public static int addTaintInt(int val, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintInt(const u4* args,
    JValue* pResult)
{
    u4 val     = args[0];
    u4 tag     = args[1];	  /* the tag to add */
    u4* rtaint = (u4*) &args[2];  /* pointer to return taint tag */
    u4 vtaint  = args[3];	  /* the existing taint tag on val */
    if ( ! isAllowed(tag, vtaint)){
      ALOGW("TaintLog: Mixing taints in addTaintInt %08x and %08x", vtaint, tag);
    }
    *rtaint = (vtaint | tag);
    RETURN_INT(val);
}

/*
 * public static int addTaintShort(short val, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintShort(const u4* args,
    JValue* pResult)
{
    u4 val     = args[0];
    u4 tag     = args[1];	  /* the tag to add */
    u4* rtaint = (u4*) &args[2];  /* pointer to return taint tag */
    u4 vtaint  = args[3];	  /* the existing taint tag on val */
    if ( ! isAllowed(tag, vtaint)){
      ALOGW("TaintLog: Mixing taints in addTaintShort %08x and %08x", vtaint, tag);
    }
    *rtaint = (vtaint | tag);
    RETURN_SHORT(val);
}

/*
 * public static long addTaintLong(long val, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintLong(const u4* args,
    JValue* pResult)
{
    u8 val;
    u4 tag     = args[2];	     /* the tag to add */
    u4* rtaint = (u4*) &args[3];     /* pointer to return taint tag */
    u4 vtaint  = args[4];	     /* the existing taint tag on val */
    memcpy(&val, &args[0], 8);	     /* EABI prevents direct store */
    if ( ! isAllowed(tag, vtaint)){
      ALOGW("TaintLog: Mixing taints in addTaintLong %08x and %08x", vtaint, tag);
    }
    *rtaint = (vtaint | tag);
    RETURN_LONG(val);
}

/*
 * public static long resetTaintLong(long val, int tag)
 */
static void Dalvik_dalvik_system_Taint_resetTaintLong(const u4* args,
    JValue* pResult)
{
    u8 val;
    u4 tag     = args[2];	     /* the tag to add */
    u4* rtaint = (u4*) &args[3];     /* pointer to return taint tag */
    u4 vtaint  = args[4];	     /* the existing taint tag on val */
    memcpy(&val, &args[0], 8);	     /* EABI prevents direct store */
    *rtaint = 0;
    RETURN_LONG(val);
}

/*
 * public static float addTaintFloat(float val, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintFloat(const u4* args,
    JValue* pResult)
{
    u4 val     = args[0];
    u4 tag     = args[1];	  /* the tag to add */
    u4* rtaint = (u4*) &args[2];  /* pointer to return taint tag */
    u4 vtaint  = args[3];	  /* the existing taint tag on val */
    if ( ! isAllowed(tag, vtaint)){
      ALOGW("TaintLog: Mixing taints in addTaintFloat %08x and %08x", vtaint, tag);
    }
    *rtaint = (vtaint | tag);
    RETURN_INT(val);		  /* Be opaque; RETURN_FLOAT doesn't work */
}

/*
 * public static double addTaintDouble(double val, int tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintDouble(const u4* args,
    JValue* pResult)
{
    u8 val;
    u4 tag     = args[2];	     /* the tag to add */
    u4* rtaint = (u4*) &args[3];     /* pointer to return taint tag */
    u4 vtaint  = args[4];	     /* the existing taint tag on val */
    memcpy(&val, &args[0], 8);	     /* EABI prevents direct store */
    if ( ! isAllowed(tag, vtaint)){
      ALOGW("TaintLog: Mixing taints in addTaintDouble %08x and %08x", vtaint, tag);
    }
    *rtaint = (vtaint | tag);
    RETURN_LONG(val);		     /* Be opaque; RETURN_DOUBLE doesn't work */
}

/*
 * public static int getTaintString(String str)
 */
static void Dalvik_dalvik_system_Taint_getTaintString(const u4* args,
    JValue* pResult)
{
    StringObject *strObj = (StringObject*) args[0];
    ArrayObject *value = NULL;

    if (strObj) {
    value = strObj->array();
	RETURN_INT(value->taint.tag);
    } else {
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int resetTaintString(String str)
 */
static void Dalvik_dalvik_system_Taint_resetTaintString(const u4* args,
    JValue* pResult)
{
	StringObject *strObj = (StringObject*) args[0];
	char * s = dvmCreateCstrFromString(strObj);
	ArrayObject *value = NULL;

	if (strObj) {
		value = strObj->array();
		value->taint.tag = 0;
	}
	free(s);
	RETURN_VOID();
}

/*
 * public static int getTaintObjectArray(Object[] obj)
 */
static void Dalvik_dalvik_system_Taint_getTaintObjectArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    if (arr) {
	RETURN_INT(arr->taint.tag);
    } else {
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int getTaintBooleanArray(boolean[] array)
 */
static void Dalvik_dalvik_system_Taint_getTaintBooleanArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    if (arr) {
	RETURN_INT(arr->taint.tag);
    } else {
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int getTaintCharArray(char[] array)
 */
static void Dalvik_dalvik_system_Taint_getTaintCharArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    if (arr) {
	RETURN_INT(arr->taint.tag);
    } else {
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int getTaintByteArray(byte[] array)
 */
static void Dalvik_dalvik_system_Taint_getTaintByteArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    if (arr) {
	RETURN_INT(arr->taint.tag);
    } else {
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int getTaintIntArray(int[] array)
 */
static void Dalvik_dalvik_system_Taint_getTaintIntArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    if (arr) {
	RETURN_INT(arr->taint.tag);
    } else {
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int getTaintShortArray(short[] array)
 */
static void Dalvik_dalvik_system_Taint_getTaintShortArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    if (arr) {
	RETURN_INT(arr->taint.tag);
    } else {
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int getTaintLongArray(long[] array)
 */
static void Dalvik_dalvik_system_Taint_getTaintLongArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    if (arr) {
	RETURN_INT(arr->taint.tag);
    } else {
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int getTaintFloatArray(float[] array)
 */
static void Dalvik_dalvik_system_Taint_getTaintFloatArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    if (arr) {
	RETURN_INT(arr->taint.tag);
    } else {
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int getTaintDoubleArray(double[] array)
 */
static void Dalvik_dalvik_system_Taint_getTaintDoubleArray(const u4* args,
    JValue* pResult)
{
    ArrayObject *arr = (ArrayObject *) args[0];
    if (arr) {
	RETURN_INT(arr->taint.tag);
    } else{
	RETURN_INT(TAINT_CLEAR);
    }
}

/*
 * public static int getTaintBoolean(boolean val)
 */
static void Dalvik_dalvik_system_Taint_getTaintBoolean(const u4* args,
    JValue* pResult)
{
    // args[0] = the value
    // args[1] = the return taint
    u4 tag = args[2]; /* the existing taint */
    RETURN_INT(tag);
}

/*
 * public static int getTaintChar(char val)
 */
static void Dalvik_dalvik_system_Taint_getTaintChar(const u4* args,
    JValue* pResult)
{
    // args[0] = the value
    // args[1] = the return taint
    u4 tag = args[2]; /* the existing taint */
    RETURN_INT(tag);
}

/*
 * public static int getTaintByte(byte val)
 */
static void Dalvik_dalvik_system_Taint_getTaintByte(const u4* args,
    JValue* pResult)
{
    // args[0] = the value
    // args[1] = the return taint
    u4 tag = args[2]; /* the existing taint */
    RETURN_INT(tag);
}

/*
 * public static int getTaintInt(int val)
 */
static void Dalvik_dalvik_system_Taint_getTaintInt(const u4* args,
    JValue* pResult)
{
    // args[0] = the value
    // args[1] = the return taint
    u4 tag = args[2]; /* the existing taint */
    RETURN_INT(tag);
}

/*
 * public static int getTaintShort(int val)
 */
static void Dalvik_dalvik_system_Taint_getTaintShort(const u4* args,
    JValue* pResult)
{
    // args[0] = the value
    // args[1] = the return taint
    u4 tag = args[2]; /* the existing taint */
    RETURN_INT(tag);
}

/*
 * public static int getTaintLong(long val)
 */
static void Dalvik_dalvik_system_Taint_getTaintLong(const u4* args,
    JValue* pResult)
{
    // args[0:1] = the value
    // args[2] = the return taint
    u4 tag = args[3]; /* the existing taint */
    RETURN_INT(tag);
}

/*
 * public static int getTaintFloat(float val)
 */
static void Dalvik_dalvik_system_Taint_getTaintFloat(const u4* args,
    JValue* pResult)
{
    // args[0] = the value
    // args[1] = the return taint
    u4 tag = args[2]; /* the existing taint */
    RETURN_INT(tag);
}

/*
 * public static int getTaintDouble(long val)
 */
static void Dalvik_dalvik_system_Taint_getTaintDouble(const u4* args,
    JValue* pResult)
{
    // args[0:1] = the value
    // args[2] = the return taint
    u4 tag = args[3]; /* the existing taint */
    RETURN_INT(tag);
}

/*
 * public static int getTaintRef(Object obj)
 */
static void Dalvik_dalvik_system_Taint_getTaintRef(const u4* args,
    JValue* pResult)
{
    // args[0] = the value
    // args[1] = the return taint
    u4 tag = args[2]; /* the existing taint */
    RETURN_INT(tag);
}

static u4 getTaintXattr(int fd)
{
    int ret;
    u4 buf;
    u4 tag = TAINT_CLEAR;

    ret = fgetxattr(fd, TAINT_XATTR_NAME, &buf, sizeof(buf));
    if (ret > 0) {
	tag = buf;
    } else {
	if (errno == ENOATTR) {
	    /* do nothing */
	} else if (errno == ERANGE) {
	    ALOGW("TaintLog: fgetxattr(%d) contents to large", fd);
	} else if (errno == ENOTSUP) {
	    /* XATTRs are not supported. No need to spam the logs */
	} else if (errno == EPERM) {
	    /* Strange interaction with /dev/log/main. Suppress the log */
	} else {
	    ALOGW("TaintLog: fgetxattr(%d): unknown error code %d", fd, errno);
	}
    }

    return tag;
}

static void setTaintXattr(int fd, u4 tag)
{
    int ret;

    ret = fsetxattr(fd, TAINT_XATTR_NAME, &tag, sizeof(tag), 0);

    if (ret < 0) {
	if (errno == ENOSPC || errno == EDQUOT) {
	    ALOGW("TaintLog: fsetxattr(%d): not enough room to set xattr", fd);
	} else if (errno == ENOTSUP) {
	    /* XATTRs are not supported. No need to spam the logs */
	} else if (errno == EPERM) {
	    /* Strange interaction with /dev/log/main. Suppress the log */
	} else {
	    ALOGW("TaintLog: fsetxattr(%d): unknown error code %d", fd, errno);
	}
    }
}

/*
 * public static int getTaintFile(int fd)
 */
static void Dalvik_dalvik_system_Taint_getTaintFile(const u4* args,
    JValue* pResult)
{
    u4 tag;
    int fd = (int)args[0]; // args[0] = the file descriptor
    // args[1] = the return taint
    // args[2] = fd taint

    tag = getTaintXattr(fd);

    if (tag) {
	ALOGI("TaintLog: getTaintFile(%d) = 0x%08x", fd, tag);
    }
    RETURN_INT(tag);
}

/*
 * public static int addTaintFile(int fd, u4 tag)
 */
static void Dalvik_dalvik_system_Taint_addTaintFile(const u4* args,
    JValue* pResult)
{
    u4 otag;
    int fd = (int)args[0]; // args[0] = the file descriptor
    u4 tag = args[1];      // args[1] = the taint tag
    // args[2] = the return taint
    // args[3] = fd taint
    // args[4] = tag taint

    otag = getTaintXattr(fd);

    if (tag) {
	ALOGI("TaintLog: addTaintFile(%d): adding 0x%08x to 0x%08x = 0x%08x",
		fd, tag, otag, tag | otag);
    }
    if ( ! isAllowed(tag, otag)){
      ALOGW("TaintLog: Mixing taints in addTaintFile %08x and %08x", otag, tag);
    }

    setTaintXattr(fd, tag | otag);

    RETURN_VOID();
}

/*
 * public static void log(String msg)
 */
static void Dalvik_dalvik_system_Taint_log(const u4* args,
    JValue* pResult)
{
    StringObject* msgObj = (StringObject*) args[0];
    char *msg;

    if (msgObj == NULL) {
	dvmThrowNullPointerException("msgObj == NULL");
	RETURN_VOID();
    }

	msg = dvmCreateCstrFromString(msgObj);
	ALOG(LOG_WARN, "TaintLog", "%s", msg);
	char *curmsg = msg;
	while(strlen(curmsg) > 1013)
	{   
		curmsg = curmsg+1013;
		ALOG(LOG_WARN, "TaintLog", "%s", curmsg);
	}
	free(msg);

    RETURN_VOID();
}

/*
 * public static void logPathFromFd(int fd)
 */
static void Dalvik_dalvik_system_Taint_logPathFromFd(const u4* args,
    JValue* pResult)
{
    int fd = (int) args[0];
    pid_t pid;
    char ppath[20]; // these path lengths should be enough
    char rpath[80];
    int err;


    pid = getpid();
    snprintf(ppath, 20, "/proc/%d/fd/%d", pid, fd);
    err = readlink(ppath, rpath, 80);
    if (err >= 0) {
	ALOGW("TaintLog: fd %d -> %s", fd, rpath);
    } else {
	ALOGW("TaintLog: error finding path for fd %d", fd);
    }

    RETURN_VOID();
}

/*
 * public static void isNotTaintable(int fd)
 */
static void Dalvik_dalvik_system_Taint_isNotTaintable(const u4* args,
    JValue* pResult)
{
    int err;
    int fd = (int) args[0];
    pid_t pid;
    char ppath[20]; // these path lengths should be enough
    char rpath[80];

    pid = getpid();
    snprintf(ppath, 20, "/proc/%d/fd/%d", pid, fd);
    err = readlink(ppath, rpath, 80);
        if (strstr(rpath,"TaintDroidInternetSrc") != NULL)
          RETURN_INT(1);
        else
          RETURN_INT(0);
}

static void Dalvik_dalvik_system_Taint_gettaint(const u4* args,
		JValue* pResult) {
	int pid = (int) args[0];
	int x = syscall(SYS_gettaint, pid);
	RETURN_INT(x);
}

static void Dalvik_dalvik_system_Taint_settaint(const u4* args,
		JValue* pResult) {
	int pid = (int) args[0];
	int tag = (int) args[1];
	int x = syscall(SYS_settaint, pid, tag);
	RETURN_INT(x);
}

static void Dalvik_dalvik_system_Taint_checkpolicy(const u4* args,
    JValue* pResult) {
	RETURN_INT(checkpolicy((int) args[0] ,(int) args[1] ,(int) args[2]));
}

static void Dalvik_dalvik_system_Taint_isAllowed(const u4* args,
    JValue* pResult) {
	RETURN_BOOLEAN(isAllowed((int) args[0] ,(int) args[1]));
}

// return 0 on success and -1 on failure
static void Dalvik_dalvik_system_Taint_setnextprocesstaint(const u4* args,
    JValue* pResult) {

	int r, fd;
	struct stat * fdstat;
	struct object_t utag;
	StringObject* nameStr = (StringObject*) args[0];
	utag.tag = (int) args[1];
	utag.name = dvmCreateCstrFromString(nameStr);

//	ALOGI("Taintlog: setnextprocesstaint for package %s", utag.name);

//	if ((fd = open(AMNIDROID_DEVICE, O_RDWR)) < 0) {
//		ALOGE("Taintlog: setnextprocesstaint failure while opening taintctrl device");
//		r = -1;
//		goto result;
//	}
	if (!(fd=get_taintctrl_dev_fd())){
		r = -1;
		goto result;
	}

//	fstat(fd, fdstat);
//	if (!S_ISCHR(fdstat->st_mode))
//	{
//		ALOGE("Taintlog: File descriptor %d is not a char device", fd);
//	
//	}

	if(ioctl(fd, AMNIDROID_SET_NEXT_TAG, &utag) < 0){
		ALOGE("Taintlog: setnextprocesstaint failure while requesting taintctrl device");
		r = -1;
		goto result;
	}

//	close(fd);
//	if (! close(fd))
//		ALOGE("Taintlog: setnextprocesstaint failure while closing device");

	r = 0;

	//ALOGI("Taintlog: setnextprocesstaint setting tag %d for namespace %s", utag.tag, utag.name);
result:
	free(utag.name);
	RETURN_INT(r);

//RETURN_INT(0);
}

// return the taint on success and -1 on failure
static void Dalvik_dalvik_system_Taint_getnextprocesstaint(const u4* args,
    JValue* pResult) {

	int r, fd;
	StringObject* nameStr = (StringObject*) args[0];
	struct object_t utag;
	utag.name = dvmCreateCstrFromString(nameStr);

//	ALOGI("Taintlog: getnextprocesstaint for package %s", utag.name);

//	if ((fd = open(AMNIDROID_DEVICE, O_RDWR)) < 0) {
//		ALOGE("Taintlog: getnextprocesstaint failure while opening taintctrl device");
//		r = -1;
//		goto result;
//	}
	
	if (!(fd = get_taintctrl_dev_fd())){
		ALOGE("Taintlog: getnextprocesstaint failure while opening device");
		r = -1;
		goto result;
	}

	//ALOGI("Taintlog: getnextprocesstaint performing ioctl");
	if(ioctl(fd, AMNIDROID_GET_NEXT_TAG, &utag) < 0){
		ALOGE("Taintlog: getnextprocesstaint failure while requesting taintctrl device");
		r = -1;
		goto result;
	}

//	close(fd);
//	if (! close(fd))
//		ALOGE("Taintlog: getnextprocesstaint failure while closing device");

	//ALOGI("Taintlog: getnextprocesstaint returning tag %d for namespace %s", utag.tag, utag.name);
	r = utag.tag;

result:
	//free(utag.name);
	RETURN_INT(r);

//RETURN_INT(0);
}

/*
// read swirls database and return the socket taint
static void Dalvik_dalvik_system_Taint_getsockettaintbyname(const u4* args,
    JValue* pResult)
{

	int r, fd;
	StringObject* nameStr = (StringObject*) args[0];
	struct object_t utag;
	utag.name = dvmCreateCstrFromString(nameStr);

//	ALOGE("Taintlog: getsockettaintbyname requesting taintctrl device %s", utag.name);
//	if ((fd = open(AMNIDROID_DEVICE, O_RDWR)) < 0) {
//		ALOGE("Taintlog: getsockettaintbyname failure while opening taintctrl device");
//		r = -1;
//		goto result;
//	}

	if (!(fd = get_taintctrl_dev_fd())){
		r = -1;
		goto result;
	}

	if(ioctl(fd, AMNIDROID_GET_SOCKET_TAG, &utag) < 0){
		ALOGE("Taintlog: getsockettaintbyname failure while requesting taintctrl device");
		r = -1;
		goto result;
	}

//	if (! close(fd))
//		ALOGE("Taintlog: getsockettaintbyname failure while closing device");

	//ALOGI("Taintlog: getsockettaintbyname returning tag %d for endpoint %s", utag.tag, utag.name);
	r = utag.tag;

result:
	//free(utag.name);
	RETURN_INT(r);

//RETURN_INT(0);
}
*/

// read swirls database and return the socket taint
static void Dalvik_dalvik_system_Taint_getsockettaintbyname(const u4* args,
    JValue* pResult)
{
	int r = 0;
	int i = 0;
	char buff[200];
	char rcn[200];
	StringObject* nameStr = (StringObject*) args[0];
	char * cn = dvmCreateCstrFromString(nameStr);

	ALOGI("Taintlog: getsockettaintbyname requesting taintctrl device %s", cn);
	FILE * f = fopen("/data/local/tmp/networktaints.log","r");
	if (f != NULL)
	{
		bzero(buff, 200);
		bzero(rcn, 200);

		while ( fgets(buff, 200, f) != NULL ){
			sscanf(buff, "%i %s", &i, rcn);
			if (! strcmp(rcn, cn)){
				r = i;
				goto result;
			}
		}
		fclose(f);
	}

	f = fopen("/data/local/tmp/networktaints.log","a+");
	if (f != NULL)
	{
		if (!i) {
			i = 1 << 16; // we start with TAINT_SRC16
			r = i;
		}
		else
			r = i<<1;
		ALOGI("Taintlog: getsockettaintbyname adding association %s %i", cn, r);
		fprintf(f, "%i %s\n", r, cn);
	}

result:
	ALOGI("Taintlog: getsockettaintbyname returning tag %d", r);
	if (f != NULL)
		fclose(f);
	RETURN_INT(r);
}

/*
//static void Dalvik_dalvik_system_Taint_assignInternetSource(JNIEnv* env, jobject obj, jobject fileDescriptor)
static void Dalvik_dalvik_system_Taint_assignInternetSource(const u4* args, JValue* pResult)
{
	struct IPtoTaint {
		char * IP;
		int Taint;
	};
	int fd = (int) args[0];
	StringObject *strObj = (StringObject*) args[0];
	ArrayObject *value = NULL;
	if (strObj) {
		value = (ArrayObject*) dvmGetFieldObject((Object*)strObj,
				gDvm.offJavaLangString_value);

		char addrStr[20] = (char*) args[1];
		ALOGI("TaintLog: new FileDescriptor %d", fd);
		int tag;
		int known_src=0;
		char current_ip[16];
		struct sockaddr_in addr;
		socklen_t addr_len = sizeof(addr);
		int last_taint_used=0;

		if ( getpeername(fd, (struct sockaddr*)&addr, &addr_len) != 0 ) {
			ALOGW("TaintLog: fd: %d getpeername faild",fd);
			tag=TAINT_CLEAR;
		}
		else {
			struct IPtoTaint known_ips[500];
			snprintf(current_ip, 20, "%s", inet_ntoa(addr.sin_addr));
			ALOGW("TaintLog: Current IP : %s ",current_ip);

			FILE * f = fopen("/data/local/tmp/TaintDroidInternetSrc.log","r");
			if (f != NULL)
			{
				char buff[20];
				int i=0;
				while ( fgets(buff, 20, f) != NULL ){
					known_ips[i].IP = (char*) malloc(20*sizeof(char));
					sscanf(buff, "%i %s", &(known_ips[i].Taint), known_ips[i].IP);
					// printf("current read entry: %i %s\n", known_ips[i].Taint, known_ips[i].IP);
					if (! strcmp(current_ip, known_ips[i].IP)){
						last_taint_used=known_ips[i].Taint-1;
						known_src=1;
						break;
					}
					last_taint_used = known_ips[i].Taint;
					i++;
					// last_taint_used = (last_taint_used < known_ips[i].Taint) ? known_ips[i].Taint : last_taint_used;
				}
				int j;
				for (j=0; j<i; j++){
					//      printf("current entry: %i %s\n",known_ips[j].Taint, known_ips[j].IP);
					free(known_ips[j].IP);
				}
				fclose(f);

			}

			switch (last_taint_used){
				case 0:
					tag=TAINT_SRC1;
					break;
				case 1:
					tag=TAINT_SRC2;
					break;
				case 2:
					tag=TAINT_SRC3;
					break;
				case 3:
					tag=TAINT_SRC4;
					break;
				case 4:
					tag=TAINT_SRC5;
					break;
				case 5:
					tag=TAINT_SRC6;
					break;
				case 6:
					tag=TAINT_SRC7;
					break;
				case 7:
					tag=TAINT_SRC8;
					break;
				case 8:
					tag=TAINT_SRC9;
					break;
				case 9:
					tag=TAINT_SRC10;
					break;
				case 10:
					tag=TAINT_SRC11;
					break;
				case 11:
					tag=TAINT_SRC12;
					break;
				case 12:
					tag=TAINT_SRC13;
					break;
				case 13:
					tag=TAINT_SRC14;
					break;
				case 14:
					tag=TAINT_SRC15;
					break;
				case 15:
					tag=TAINT_SRC16;
					break;
				case 16:
					tag=TAINT_SRC1;
					break;
			}

			if (!known_src){
				FILE * g = fopen("/data/local/tmp/TaintDroidInternetSrc.log","a+");
				if (g != NULL){
					fprintf(g,"%d %s\n",++last_taint_used,current_ip);
					fclose(g);
					LOGI("TaintLog: new src %s with tag %d",current_ip, last_taint_used);
				}
				else {
					LOGI("TaintLog: IP Source log couldn't be created");
				}
			}
		}
	}
	RETURN_INT(tag);
}
*/


/*
 * public static void logPeerFromFd(int fd)
 */
static void Dalvik_dalvik_system_Taint_logPeerFromFd(const u4* args,
    JValue* pResult)
{
    int fd = (int) args[0];

    ALOGW("TaintLog: logPeerFromFd not yet implemented");

    RETURN_VOID();
}

const DalvikNativeMethod dvm_dalvik_system_Taint[] = {
    { "getpid",  "(V)I",
        Dalvik_dalvik_system_Taint_getpid},
    { "addTaintString",  "(Ljava/lang/String;I)V",
        Dalvik_dalvik_system_Taint_addTaintString},
    { "addTaintObjectArray",  "([Ljava/lang/Object;I)V",
        Dalvik_dalvik_system_Taint_addTaintObjectArray},
    { "addTaintBooleanArray",  "([ZI)V",
        Dalvik_dalvik_system_Taint_addTaintBooleanArray},
    { "addTaintCharArray",  "([CI)V",
        Dalvik_dalvik_system_Taint_addTaintCharArray},
    { "addTaintByteArray",  "([BI)V",
        Dalvik_dalvik_system_Taint_addTaintByteArray},
    { "addTaintIntArray",  "([II)V",
        Dalvik_dalvik_system_Taint_addTaintIntArray},
    { "addTaintShortArray",  "([SI)V",
        Dalvik_dalvik_system_Taint_addTaintShortArray},
    { "addTaintLongArray",  "([JI)V",
        Dalvik_dalvik_system_Taint_addTaintLongArray},
    { "addTaintFloatArray",  "([FI)V",
        Dalvik_dalvik_system_Taint_addTaintFloatArray},
    { "addTaintDoubleArray",  "([DI)V",
        Dalvik_dalvik_system_Taint_addTaintDoubleArray},
    { "addTaintBoolean",  "(ZI)Z",
        Dalvik_dalvik_system_Taint_addTaintBoolean},
    { "addTaintChar",  "(CI)C",
        Dalvik_dalvik_system_Taint_addTaintChar},
    { "addTaintByte",  "(BI)B",
        Dalvik_dalvik_system_Taint_addTaintByte},
    { "addTaintInt",  "(II)I",
        Dalvik_dalvik_system_Taint_addTaintInt},
    { "addTaintShort",  "(SI)S",
        Dalvik_dalvik_system_Taint_addTaintShort},
    { "addTaintLong",  "(JI)J",
        Dalvik_dalvik_system_Taint_addTaintLong},
    { "addTaintFloat",  "(FI)F",
        Dalvik_dalvik_system_Taint_addTaintFloat},
    { "addTaintDouble",  "(DI)D",
        Dalvik_dalvik_system_Taint_addTaintDouble},
    { "getTaintString",  "(Ljava/lang/String;)I",
        Dalvik_dalvik_system_Taint_getTaintString},
    { "resetTaintString",  "(Ljava/lang/String;)I",
        Dalvik_dalvik_system_Taint_resetTaintString},
    { "getTaintObjectArray",  "([Ljava/lang/Object;)I",
        Dalvik_dalvik_system_Taint_getTaintObjectArray},
    { "getTaintBooleanArray",  "([Z)I",
        Dalvik_dalvik_system_Taint_getTaintBooleanArray},
    { "getTaintCharArray",  "([C)I",
        Dalvik_dalvik_system_Taint_getTaintCharArray},
    { "getTaintByteArray",  "([B)I",
        Dalvik_dalvik_system_Taint_getTaintByteArray},
    { "getTaintIntArray",  "([I)I",
        Dalvik_dalvik_system_Taint_getTaintIntArray},
    { "getTaintShortArray",  "([S)I",
        Dalvik_dalvik_system_Taint_getTaintShortArray},
    { "getTaintLongArray",  "([J)I",
        Dalvik_dalvik_system_Taint_getTaintLongArray},
    { "getTaintFloatArray",  "([F)I",
        Dalvik_dalvik_system_Taint_getTaintFloatArray},
    { "getTaintDoubleArray",  "([D)I",
        Dalvik_dalvik_system_Taint_getTaintDoubleArray},
    { "getTaintBoolean",  "(Z)I",
        Dalvik_dalvik_system_Taint_getTaintBoolean},
    { "getTaintChar",  "(C)I",
        Dalvik_dalvik_system_Taint_getTaintChar},
    { "getTaintByte",  "(B)I",
        Dalvik_dalvik_system_Taint_getTaintByte},
    { "getTaintInt",  "(I)I",
        Dalvik_dalvik_system_Taint_getTaintInt},
    { "getTaintShort",  "(S)I",
        Dalvik_dalvik_system_Taint_getTaintShort},
    { "getTaintLong",  "(J)I",
        Dalvik_dalvik_system_Taint_getTaintLong},
    { "resetTaintLong",  "(JI)J",
        Dalvik_dalvik_system_Taint_resetTaintLong},
    { "getTaintFloat",  "(F)I",
        Dalvik_dalvik_system_Taint_getTaintFloat},
    { "getTaintDouble",  "(D)I",
        Dalvik_dalvik_system_Taint_getTaintDouble},
    { "getTaintRef",  "(Ljava/lang/Object;)I",
        Dalvik_dalvik_system_Taint_getTaintRef},
    { "getTaintFile",  "(I)I",
        Dalvik_dalvik_system_Taint_getTaintFile},
    { "addTaintFile",  "(II)V",
        Dalvik_dalvik_system_Taint_addTaintFile},
    { "log",  "(Ljava/lang/String;)V",
        Dalvik_dalvik_system_Taint_log},
    { "logPathFromFd",  "(I)V",
        Dalvik_dalvik_system_Taint_logPathFromFd},
    { "isNotTaintable",  "(I)I",
        Dalvik_dalvik_system_Taint_isNotTaintable},
    { "gettaint",  "(I)I",
        Dalvik_dalvik_system_Taint_gettaint},
    { "settaint",  "(II)I",
        Dalvik_dalvik_system_Taint_settaint},
    { "checkpolicy",  "(III)I",
        Dalvik_dalvik_system_Taint_checkpolicy},
    { "isAllowed",  "(II)Z",
        Dalvik_dalvik_system_Taint_isAllowed},
    { "setnextprocesstaint",  "(Ljava/lang/string;I)I",
        Dalvik_dalvik_system_Taint_setnextprocesstaint},
    { "getnextprocesstaint",  "(Ljava/lang/String;)I",
        Dalvik_dalvik_system_Taint_getnextprocesstaint},
    { "getsockettaintbyname",  "(Ljava/lang/String;)I",
        Dalvik_dalvik_system_Taint_getsockettaintbyname},
    { "logPeerFromFd",  "(I)V",
        Dalvik_dalvik_system_Taint_logPeerFromFd},
    { NULL, NULL, NULL },
};
